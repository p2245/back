const express = require('express');
const app = express();
const CalificationRoute = express.Router();

// Calification model
let Calification = require('../models/Calification');

// Add Calification
CalificationRoute.route('/create').post((req, res, next) => {
  Calification.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

// Get All Califications
CalificationRoute.route('/').get((req, res) => {
  Calification.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get single Calification
CalificationRoute.route('/read/:id').get((req, res) => {
  Calification.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Update Calification
CalificationRoute.route('/update/:id').put((req, res, next) => {
  Calification.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('Data updated successfully')
    }
  })
})

// Delete Calification
CalificationRoute.route('/delete/:id').delete((req, res, next) => {
  Calification.findOneAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = CalificationRoute;