const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let Calification = new Schema({
    name: {
        type: String
    },
    grade: {
        type: String
    },
    group: {
        type: String
    },
    materia: {
        type: String
    },
    qualification: {
        type: String
    }
}, {
    collection: 'calification'
})

module.exports = mongoose.model('Calification', Calification)